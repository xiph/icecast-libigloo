if [ "z$OBS_BASES" = "z" ]; then
  if [ "z$CI_COMMIT_TAG" != "z" ]; then
    if echo $CI_COMMIT_TAG | grep -q '^v[0-9]\+\.[0-9]\+\.[0-9]\+$'; then
      export OBS_BASES="$OBS_BASE_RELEASE $OBS_BASE_BETA"
    else
      export OBS_BASES="$OBS_BASE_BETA"
    fi
  else
    echo "tag variable CI_COMMIT_TAG not defined, please export OBS_BASES accordingly";
    exit 1
  fi
fi

export LIBIGLOO_VERSION=0.9.3
export LIBIGLOO_CI_VERSION=$LIBIGLOO_VERSION
export DISABLE_CHANGELOG=1
export RELEASE_AUTHOR="Philipp Schafft <phschafft@de.loewenfelsen.net>"
export RELEASE_DATETIME=2025-01-24T19:46:16+00:00

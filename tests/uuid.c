/* Copyright (C) 2022       Philipp "ph3-der-loewe" Schafft <lion@lion.leolix.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <igloo/tap.h>
#include <igloo/igloo.h>
#include <igloo/uuid.h>
#include <igloo/sp.h>

static igloo_ro_t g_instance;

static void check_uuid(const char *ref)
{
    size_t len = strlen(ref);
    igloo_tap_test("strlen(ref) == 36", len == 36);

    igloo_tap_diagnostic("Ref is:");
    igloo_tap_diagnostic(ref);

    if (len == 36) {
        igloo_tap_test_success("igloo_uuid_is_valid", igloo_uuid_is_valid(ref, igloo_UUID_STRICTNESS_STRICT));
    }
}

static void group_valid(void)
{
    static const struct {
        const char *str;
        igloo_uuid_strictness_t strictness;
        igloo_error_t error;
    } vectors[] = {
        /* invalid group length */
        {"551a9547-fe63-4cf-a153-2801bac47ff8", igloo_UUID_STRICTNESS_STRICT, igloo_ERROR_ILLSEQ},
        {"551a9547-fe63-4cf-a153-2801bac47ff8", igloo_UUID_STRICTNESS_RELAXED, igloo_ERROR_ILLSEQ},
        {"551a9547-fe63-4cf-a153-2801bac47ff8", igloo_UUID_STRICTNESS_LOOSE, igloo_ERROR_ILLSEQ},

        {"551a9547-fe63-45cf-a153-2801bac47", igloo_UUID_STRICTNESS_STRICT, igloo_ERROR_ILLSEQ},
        {"551a9547-fe63-45cf-a153-2801bac47", igloo_UUID_STRICTNESS_RELAXED, igloo_ERROR_ILLSEQ},
        {"551a9547-fe63-45cf-a153-2801bac47", igloo_UUID_STRICTNESS_LOOSE, igloo_ERROR_ILLSEQ},

        /* invalid characters */
        {"551a9547-fe63-4zcf-a153-2801bac47ff8", igloo_UUID_STRICTNESS_STRICT, igloo_ERROR_ILLSEQ},
        {"551a9547-fe63-4zcf-a153-2801bac47ff8", igloo_UUID_STRICTNESS_RELAXED, igloo_ERROR_ILLSEQ},
        {"551a9547-fe63-4zcf-a153-2801bac47ff8", igloo_UUID_STRICTNESS_LOOSE, igloo_ERROR_ILLSEQ},
        {"551a9547-fe63-4 cf-a153-2801bac47ff8", igloo_UUID_STRICTNESS_STRICT, igloo_ERROR_ILLSEQ},
        {"551a9547-fe63-4 cf-a153-2801bac47ff8", igloo_UUID_STRICTNESS_RELAXED, igloo_ERROR_ILLSEQ},
        {"551a9547-fe63-4 cf-a153-2801bac47ff8", igloo_UUID_STRICTNESS_LOOSE, igloo_ERROR_ILLSEQ},

        /* all fine */
        {"551a9547-fe63-45cf-a153-2801bac47ff8", igloo_UUID_STRICTNESS_STRICT, igloo_ERROR_NONE},
        {"551a9547-fe63-45cf-a153-2801bac47ff8", igloo_UUID_STRICTNESS_RELAXED, igloo_ERROR_NONE},
        {"551a9547-fe63-45cf-a153-2801bac47ff8", igloo_UUID_STRICTNESS_LOOSE, igloo_ERROR_NONE},

        /* upper case */
        {"551a9547-FE63-45cf-a153-2801bac47ff8", igloo_UUID_STRICTNESS_STRICT, igloo_ERROR_ILLSEQ},
        {"551a9547-FE63-45cf-a153-2801bac47ff8", igloo_UUID_STRICTNESS_RELAXED, igloo_ERROR_NONE},
        {"551a9547-FE63-45cf-a153-2801bac47ff8", igloo_UUID_STRICTNESS_LOOSE, igloo_ERROR_NONE},

        /* spaces */
        {" 551a9547-fe63-45cf-a153-2801bac47ff8", igloo_UUID_STRICTNESS_STRICT, igloo_ERROR_ILLSEQ},
        {" 551a9547-fe63-45cf-a153-2801bac47ff8", igloo_UUID_STRICTNESS_RELAXED, igloo_ERROR_ILLSEQ},
        {" 551a9547-fe63-45cf-a153-2801bac47ff8", igloo_UUID_STRICTNESS_LOOSE, igloo_ERROR_NONE},
        {"551a9547-fe63-45cf-a153-2801bac47ff8 ", igloo_UUID_STRICTNESS_STRICT, igloo_ERROR_ILLSEQ},
        {"551a9547-fe63-45cf-a153-2801bac47ff8 ", igloo_UUID_STRICTNESS_RELAXED, igloo_ERROR_ILLSEQ},
        {"551a9547-fe63-45cf-a153-2801bac47ff8 ", igloo_UUID_STRICTNESS_LOOSE, igloo_ERROR_NONE},

        /* spaces and upper case */
        {" 551a9547-fe63-45CF-a153-2801bac47ff8", igloo_UUID_STRICTNESS_STRICT, igloo_ERROR_ILLSEQ},
        {" 551a9547-fe63-45CF-a153-2801bac47ff8", igloo_UUID_STRICTNESS_RELAXED, igloo_ERROR_ILLSEQ},
        {" 551a9547-fe63-45CF-a153-2801bac47ff8", igloo_UUID_STRICTNESS_LOOSE, igloo_ERROR_NONE},
        {"551a9547-fe63-45CF-a153-2801bac47ff8 ", igloo_UUID_STRICTNESS_STRICT, igloo_ERROR_ILLSEQ},
        {"551a9547-fe63-45CF-a153-2801bac47ff8 ", igloo_UUID_STRICTNESS_RELAXED, igloo_ERROR_ILLSEQ},
        {"551a9547-fe63-45CF-a153-2801bac47ff8 ", igloo_UUID_STRICTNESS_LOOSE, igloo_ERROR_NONE},

        /* fences */
        {"{551a9547-fe63-45cf-a153-2801bac47ff8}", igloo_UUID_STRICTNESS_STRICT, igloo_ERROR_ILLSEQ},
        {"{551a9547-fe63-45cf-a153-2801bac47ff8}", igloo_UUID_STRICTNESS_RELAXED, igloo_ERROR_ILLSEQ},
        {"{551a9547-fe63-45cf-a153-2801bac47ff8}", igloo_UUID_STRICTNESS_LOOSE, igloo_ERROR_NONE},
        {" {551a9547-fe63-45cf-a153-2801bac47ff8} ", igloo_UUID_STRICTNESS_STRICT, igloo_ERROR_ILLSEQ},
        {" {551a9547-fe63-45cf-a153-2801bac47ff8} ", igloo_UUID_STRICTNESS_RELAXED, igloo_ERROR_ILLSEQ},
        {" {551a9547-fe63-45cf-a153-2801bac47ff8} ", igloo_UUID_STRICTNESS_LOOSE, igloo_ERROR_NONE},
        {" { 551a9547-fe63-45cf-a153-2801bac47ff8} ", igloo_UUID_STRICTNESS_STRICT, igloo_ERROR_ILLSEQ},
        {" { 551a9547-fe63-45cf-a153-2801bac47ff8} ", igloo_UUID_STRICTNESS_RELAXED, igloo_ERROR_ILLSEQ},
        {" { 551a9547-fe63-45cf-a153-2801bac47ff8} ", igloo_UUID_STRICTNESS_LOOSE, igloo_ERROR_ILLSEQ},
        {" {551a9547-fe63-45cf-a153-2801bac47ff8 } ", igloo_UUID_STRICTNESS_STRICT, igloo_ERROR_ILLSEQ},
        {" {551a9547-fe63-45cf-a153-2801bac47ff8 } ", igloo_UUID_STRICTNESS_RELAXED, igloo_ERROR_ILLSEQ},
        {" {551a9547-fe63-45cf-a153-2801bac47ff8 } ", igloo_UUID_STRICTNESS_LOOSE, igloo_ERROR_ILLSEQ},
        {" {551a9547-fe63-45cf-a153-2801bac47ff8 ", igloo_UUID_STRICTNESS_STRICT, igloo_ERROR_ILLSEQ},
        {" {551a9547-fe63-45cf-a153-2801bac47ff8 ", igloo_UUID_STRICTNESS_RELAXED, igloo_ERROR_ILLSEQ},
        {" {551a9547-fe63-45cf-a153-2801bac47ff8 ", igloo_UUID_STRICTNESS_LOOSE, igloo_ERROR_ILLSEQ},
    };

    for (size_t i = 0; i < (sizeof(vectors)/sizeof(*vectors)); i++) {
        igloo_error_t ret = igloo_uuid_is_valid(vectors[i].str, vectors[i].strictness);
        const igloo_error_desc_t * error_desc = igloo_error_get_description(vectors[i].error);
        char buf[1024] = "XXX";

        switch (vectors[i].strictness) {
            case igloo_UUID_STRICTNESS_STRICT:
                snprintf(buf, sizeof(buf), "igloo_uuid_is_valid(\"%s\", igloo_UUID_STRICTNESS_STRICT) -> %s", vectors[i].str, error_desc->name);
                break;
            case igloo_UUID_STRICTNESS_RELAXED:
                snprintf(buf, sizeof(buf), "igloo_uuid_is_valid(\"%s\", igloo_UUID_STRICTNESS_RELAXED) -> %s", vectors[i].str, error_desc->name);
                break;
            case igloo_UUID_STRICTNESS_LOOSE:
                snprintf(buf, sizeof(buf), "igloo_uuid_is_valid(\"%s\", igloo_UUID_STRICTNESS_LOOSE) -> %s", vectors[i].str, error_desc->name);
                break;
        }

        igloo_tap_test_error(buf, vectors[i].error, ret);
    }
}

static void group_sp(void)
{
    const char *ref = NULL;

    igloo_tap_test_success("igloo_uuid_new_random_sp", igloo_uuid_new_random_sp(&ref, g_instance));
    igloo_tap_test("ref != NULL", ref != NULL);
    if (ref) {
        check_uuid(ref);
        igloo_tap_test_success("igloo_sp_unref", igloo_sp_unref(&ref, g_instance));
        igloo_tap_test("ref == NULL", ref == NULL);
    }
}

static void group_cstr(void)
{
    char *ref = NULL;

    igloo_tap_test_success("igloo_uuid_new_random_cstr", igloo_uuid_new_random_cstr(&ref, g_instance));
    igloo_tap_test("ref != NULL", ref != NULL);
    if (ref) {
        check_uuid(ref);
        free(ref);
    }
}

int main (void)
{
    igloo_tap_init();
    igloo_tap_exit_on(igloo_TAP_EXIT_ON_FIN, NULL);
    igloo_tap_test_success("igloo_initialize", igloo_initialize(&g_instance));
    igloo_tap_group_run("valid", group_valid);
    igloo_tap_group_run("sp", group_sp);
    igloo_tap_group_run("cstr", group_cstr);
    igloo_tap_test_success("unref instance", igloo_ro_unref(&g_instance));
    igloo_tap_fin();

    return EXIT_FAILURE; // return failure as we should never reach this point!
}

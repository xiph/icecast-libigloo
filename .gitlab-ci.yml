variables:
  BASE_IMAGE_REGISTRY: registry.gitlab.xiph.org/xiph/icecast-docker-images
  FAST_TRACK: "false"

stages:
  - build
  - test
  - sanitizer
  - packaging
  - deploy

default:
  image: 
    name: $BASE_IMAGE_REGISTRY/debian-bookworm:20240114141402
  tags:
    - docker

# Builds

.TemplateAlpine:
  image: $BASE_IMAGE_REGISTRY/alpine-icecast-libigloo:20240113223423
  before_script:
    - set -xe
    - cat /etc/os*
      # Required for tests
      #- apk add curl ffmpeg
      # Create user to run tests
      #- adduser -s /bin/sh -D -H icecast

Build (x86_64):
  stage: build
  script:
    - gcc --version
    - ./autogen.sh
    - ./configure || { cat config.log; exit 1; }
    - make
  artifacts:
    untracked: true
    expire_in: 1 hour

Build Alpine (x86_64):
  extends: .TemplateAlpine
  rules:
    - if: $FAST_TRACK == "false"
  stage: build
  script:
    - gcc --version
    - ./autogen.sh
    - ./configure || { cat config.log; exit 1; }
    - make
  artifacts:
    untracked: true
    expire_in: 1 hour


Build (x86):
  stage: build
  rules:
    - if: $FAST_TRACK == "false"
  variables:
    CFLAGS: -m32
  script:
    - gcc --version
    - ./autogen.sh
    - ./configure || { cat config.log; exit 1; }
    - make
  artifacts:
    untracked: true
    expire_in: 1 hour

Build (clang, x86_64):
  stage: build
  rules:
    - if: $FAST_TRACK == "false"
  variables:
    CC: clang
  script:
    - clang --version
    - ./autogen.sh
    - ./configure || { cat config.log; exit 1; }
    - make
  artifacts:
    paths:
      - "config.log"
    expire_in: 1 hour

# Sanitizer builds

Sanitizer:
  rules:
    - if: $FAST_TRACK == "false"
  stage: sanitizer
  parallel:
    matrix:
      - SANITIZER: [address, thread, undefined]
        CC: [clang, gcc]
      - SANITIZER: [memory, integer]
        CC: [clang]
  script:
    - ${CC} --version
    - mkdir rhash-tmp
    - cd rhash-tmp
    - git clone -b v1.4.1 https://github.com/rhash/RHash.git rhash-1.4.1
    - cd rhash-*
    - ./configure --cc=${CC} --extra-cflags="-fsanitize=${SANITIZER} -Wdate-time -D_FORTIFY_SOURCE=2 -g -O2 -ffile-prefix-map=`pwd`=. -fstack-protector-strong -Wformat -Werror=format-security -D_FILE_OFFSET_BITS=6" --extra-ldflags="-fsanitize=${SANITIZER}  -Wl,-z,relro -Wl,-z,now" --prefix=`pwd`/../rhash-prefix --enable-lib-static --enable-gettext --enable-openssl-runtime --with-install="install -p -m 755" || { echo "---CONFIG.LOG RHASH START ---"; cat config.log; echo "---CONFIG.LOG RHASH END ---"; exit 1; } # half of this is stolen from bullseyes debian/rules
    - make install install-pkg-config install-lib-shared install-lib-so-link
    - cd ../..
    - ./autogen.sh
    - PKG_CONFIG_PATH=`pwd`/rhash-prefix/lib/pkgconfig/ ./configure --with-sanitizer=${SANITIZER} || { printf "---CONFIG.LOG LIBIGLOO START ---"; cat config.log; printf "---CONFIG.LOG LIBIGLOO END ---"; exit 1; }
    - LD_LIBRARY_PATH=`pwd`/rhash-tmp/rhash-prefix/lib make check || { printf "---TESTLOGS LIBIGLOO START ---"; find ./tests -name '*.log' -print -exec sh -c "printf '----TESTLOG LIBIGLOO %s START ----' '{}';cat '{}'; printf '----TESTLOG LIBIGLOO %s END ----' '{}' " \;; printf "---TESTLOGS LIBIGLOO END ---"; printf "---TESTSUITE LIBIGLOO START ---"; cat test-suite.log; printf "---TESTSUITE LIBIGLOO END ---"; exit 1; }
  # this statement needs to be there to disable copying of dependecies from other stage jobs - otherwhise you get funny errors - https://stackoverflow.com/a/47679028
  dependencies: []
  artifacts:
    paths:
      - rhash-tmp/rhash-*/config.log
    untracked: true
    expire_in: 1 hour

# Build and run tests

Test (x86_64):
  stage: test
  script:
    - make check || { cat test-suite.log; exit 1; }
  needs:
    - 'Build (x86_64)'
  artifacts:
    paths:
      - "test-suite.log"
    expire_in: 1 hour

Test Alpine (x86_64):
  extends: .TemplateAlpine
  rules:
    - if: $FAST_TRACK == "false"
  stage: test
  script:
    - make check || { cat test-suite.log; exit 1; }
  needs:
    - 'Build Alpine (x86_64)'
  artifacts:
    paths:
      - "test-suite.log"
    expire_in: 1 hour

Test (x86):
  rules:
    - if: $FAST_TRACK == "false"
  stage: test
  variables:
    CFLAGS: -m32
  script:
    - make check || { cat test-suite.log; exit 1; }
  needs:
    - 'Build (x86)'
  artifacts:
    paths:
      - "test-suite.log"
    expire_in: 1 hour

# Distcheck / Source tarballs

Distcheck:
  stage: test
  script:
    - make distcheck
  artifacts:
    paths:
      - libigloo-*.tar.gz
      - libigloo-*.zip
    expire_in: 1 week
  needs:
    - 'Build (x86_64)'

Package Alpine (x86_64):
  extends: .TemplateAlpine
  rules:
    - if: $FAST_TRACK == "false"
  stage: packaging
  variables:
    PACKAGE_PREFIX: "alpine-libigloo-bins"
    PACKAGE_VERSION: "0.92"
  script:
    - mkdir _install_base
    - make install DESTDIR=`pwd`/_install_base/
    - cd _install_base/
    - tar -cvzf ../$PACKAGE_PREFIX-$CI_COMMIT_REF_NAME.tar.gz *
    - cd ..
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file $PACKAGE_PREFIX-$CI_COMMIT_REF_NAME.tar.gz "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/$PACKAGE_PREFIX/$PACKAGE_VERSION-$CI_COMMIT_REF_NAME/$PACKAGE_PREFIX-$CI_COMMIT_REF_NAME.tar.gz"'
  when: manual
  needs:
    # we need the build job artifacts
    - job: 'Build Alpine (x86_64)'
      artifacts: true
    # we depend on tests for ordering
    - 'Test Alpine (x86_64)'

upload_dist:
  image: $BASE_IMAGE_REGISTRY/alpine-osc:20240114141818
  only:
    - master
    - devel

  stage: deploy

  dependencies:
    - Distcheck

  before_script:
    - ./ci/osc/prepare-osc-tools.sh

  script:
    - ./ci/osc/handle-osc-upload.sh

upload_dist_release:
  image: $BASE_IMAGE_REGISTRY/alpine-osc:20240114141818
  only:
    - tags

  stage: deploy

  dependencies:
    - Distcheck

  before_script:
    - ./ci/osc/prepare-osc-tools.sh

  script:
    - ./ci/osc/handle-osc-upload.sh release

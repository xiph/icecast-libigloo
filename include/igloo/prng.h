/* Copyright (C) 2020-2021  Philipp "ph3-der-loewe" Schafft <lion@lion.leolix.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

#ifndef _LIBIGLOO__PRNG_H_
#define _LIBIGLOO__PRNG_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <sys/types.h>

#include <igloo/types.h>

typedef uint64_t igloo_prng_flags_t;

#define igloo_PRNG_FLAG_NONE        ((igloo_prng_flags_t)0)

/* Change options */
igloo_error_t igloo_prng_configure(igloo_ro_t instance, igloo_prng_flags_t addflags, igloo_prng_flags_t removeflags, ssize_t overcommitment);
/* Run a reseed now, useful to be run at times of idle */
igloo_error_t igloo_prng_auto_reseed(igloo_ro_t instance, igloo_prng_flags_t flags);

/* write data to the PRNG to seed it */
igloo_error_t igloo_prng_write(igloo_ro_t instance, const void *buffer, size_t len, ssize_t bits, igloo_prng_flags_t flags);
/* read pseudo random bytes from the PRNG */
ssize_t igloo_prng_read(igloo_ro_t instance, void *buffer, size_t len, igloo_prng_flags_t flags);

/* write len pseudo random bytes to a file. If len is -1 a default value is used. */
igloo_error_t igloo_prng_write_file(igloo_ro_t instance, const char *filename, ssize_t len, igloo_prng_flags_t flags);
/* read at max len bytes from the file and see the PRNG with it. if len is -1 all of the file is read. */
igloo_error_t igloo_prng_read_file(igloo_ro_t instance, const char *filename, ssize_t len, ssize_t bits, igloo_prng_flags_t flags);

#ifdef __cplusplus
}
#endif

#endif /* ! _LIBIGLOO__PRNG_H_ */
